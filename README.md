# [cython](https://tracker.debian.org/pkg/cython)

The Cython compiler for writing C extensions for the Python language https://cython.org/

* https://gitlab.com/python-packages-demo/cython
* https://gitlab.com/alpinelinux-packages-demo/cython

* https://en.wikipedia.org/wiki/Cython